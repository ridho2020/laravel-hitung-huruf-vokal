<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VokalController extends Controller
{
    public function index()
    {
        return view('vokal');
    }

    public function vokal(Request $request)
    {

        $nama = strip_tags($request->input('nama'));
        $kata = strtolower($nama);
        $kata = str_replace(' ', '', $kata);
        if (is_numeric($kata)) {
            $hasil = "tidak boleh angka";
        } elseif (empty($kata)) {
            $hasil = "tidak boleh kosong";
        } else {
            $vokal = ['a', 'i', 'u', 'e', 'o'];
            $arrKata = str_split($kata);

            $cek = array_intersect($arrKata, $vokal);
            $cek = array_unique($cek);
            $string = implode($cek);
            $pecah_string = str_split($string);
            if (strlen($string) == 5) {
                $hasil = $nama . " = " . count($cek) . " yaitu " . $pecah_string[0] . " , " . $pecah_string[1] . " , " . $pecah_string[2] . " , " . $pecah_string[3] . " dan " . $pecah_string[4];
            } elseif (strlen($string) == 4) {
                $hasil = $nama . " = " . count($cek) . " yaitu " . $pecah_string[0] . " , " . $pecah_string[1] . " , " . $pecah_string[2] . " dan " . $pecah_string[3];
            } elseif (strlen($string) == 3) {
                $hasil = $nama . " = " . count($cek) . " yaitu " . $pecah_string[0] . " , " . $pecah_string[1] . " dan " . $pecah_string[2];
            } elseif (strlen($string) == 2) {
                $hasil = $nama . " = " . count($cek) . " yaitu " . $pecah_string[0] . " dan " . $pecah_string[1];
            } elseif (strlen($string) == 1) {
                $hasil = $nama . " = " . count($cek) . " yaitu hanya " . $pecah_string[0];
            } else {
                $hasil = "tidak ada huruf vokal";
            }

            return view('vokal', [
                'hasil' => $hasil
            ]);
        }
    }
}
