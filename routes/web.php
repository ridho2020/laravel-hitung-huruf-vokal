<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\VokalController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/vokal', [VokalController::class, 'index']);
Route::post('/vokal', [VokalController::class, 'vokal']);